# Soapbox DL

A Cloudflare Worker that serves GitLab CI artifacts from Soapbox on nicer URLs with more control.

https://dl.soapbox.pub

## Developing locally

```
bun install
bun run dev
```

## Deploying

```
bun run deploy
```

## License

This is free and unencumbered software released into the public domain.
