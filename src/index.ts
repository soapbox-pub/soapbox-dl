import { Hono } from 'hono';

interface Env {
  Bindings: {
    HOST: string;
    PROJECT: string;
    ARTIFACT: string;
    JOB: string;
  };
}

const app = new Hono<Env>();

app.get('/install/knox.sh', (c) => {
  return fetch('https://gitlab.com/soapbox-pub/knox/-/raw/main/install.sh');
});

app.get('/:ref/soapbox.zip', async (c) => {
  const ref = c.req.param('ref');

  const { HOST, PROJECT, ARTIFACT, JOB } = c.env;
  const { body, status } = await fetch(
    `https://${HOST}/${PROJECT}/-/jobs/artifacts/${ref}/raw/${ARTIFACT}?job=${JOB}`,
  );

  if (status !== 200) {
    return c.text('404 Not Found', 404);
  }

  return new Response(body, {
    headers: {
      'Content-Disposition': 'attachment; filename=soapbox.zip',
      'Content-Type': 'application/zip',
    }
  });
});

app.get('/', (c) => {
  return c.text(`To download Soapbox:
  
curl -O https://dl.soapbox.pub/main/soapbox.zip

Other possibilities:

- https://dl.soapbox.pub/my-branch/soapbox.zip
- https://dl.soapbox.pub/f73085fd0ab5603f0f2b1b3434efa281316ee9eb/soapbox.zip
`);
});

export default app;
